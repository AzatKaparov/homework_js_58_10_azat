import React from 'react';

const Alert = (props) => {
    if (props.show) {
        return (
            <div className={['alert', 'alert-relative', `alert-${props.type}`].join(' ')}>
                {props.dismiss
                    ? <button
                        className="alert-close btn btn-secondary"
                        onClick={props.dismiss}
                    >
                        X</button>
                    : null
                }
                {props.children}
            </div>
        );
    }  else {
        return null;
    }
};

export default Alert;