import React from 'react';

const Backdrop = ({ show, closed }) => (
    show ? <div className="Backdrop" onClick={closed} /> : null
);

export default Backdrop;