import React from 'react';
import Backdrop from "./Backdrop";

const ModalWindow = (props) => {
    let btns;
    if (props.buttons) {
        btns = props.buttons.map((item, idx) => (
            <button
                key={idx}
                onClick={item.clicked}
                className={['btn', 'mr-2', `btn-${item.type}`].join(' ')}
            >
                {item.label}
            </button>
        ));
    } else {
        btns = null;
    }

    return (
        <>
            <Backdrop show={props.show} closed={props.closed}/>
            <div
                className="Modal"
                style={{display: props.show ? 'block' : 'none'}}>
                <button
                    onClick={props.closed}
                    className="btn btn-danger close-btn"
                >
                    X
                </button>
                <h1>{props.title}</h1>
                {props.children}
                {btns}
            </div>
        </>
    );
};

export default ModalWindow;