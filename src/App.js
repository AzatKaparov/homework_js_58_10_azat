import React, { useState } from 'react';
import './App.css';
import ModalWindow from "./components/UI/ModalWindow";
import Alert from "./components/UI/Alert";

function App() {
  const [showModal, setShowModal] = useState(false);

  const modalHandler = () => {
    setShowModal(!showModal);
  };

  const [alerts, setAlerts] = useState([
      {id: 1, type: "primary", show: true},
      {id: 2, type: "warning", show: true},
  ]);

  const alertHideHandler = id => {
      setAlerts(alerts.map(item => {
          if (item.id === id) {
              return {
                  ...item,
                  show: !item.show
              };
          }
          return item;
      }));
  };

  const modalContinue = () => {
      alert("You continued!");
  };

  return (
    <div className="container">
        <button onClick={modalHandler} className="btn btn-primary mb-5">Show modal</button>
      <ModalWindow
          show={showModal}
          closed={modalHandler}
          title="Some title"
          buttons={[
              {type: 'primary', label: 'Continue', clicked: modalContinue},
              {type: 'danger', label: 'Close', clicked: modalHandler}
          ]}
      >
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, vitae!</p>
      </ModalWindow>
        {alerts.map(item => {
            return (
                <Alert
                    dismiss={() => alertHideHandler(item.id)}
                    show={item.show}
                    type={item.type}
                    key={item.id}
                >
                    <p className="m-0">Lorem ipsum dolor.</p>
                </Alert>
            )
        })}
    </div>
  );
}

export default App;
